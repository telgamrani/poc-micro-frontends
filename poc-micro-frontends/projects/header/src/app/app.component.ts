import { Component } from '@angular/core';

@Component({
  selector: 'carlo-header',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'header';
}
