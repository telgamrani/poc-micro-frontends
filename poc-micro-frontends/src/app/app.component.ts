import { Component } from '@angular/core';
import { ShellService } from './shell/shell.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'poc-micro-frontends';

  constructor(private shellService: ShellService) {}

  ngOnInit() {
    this.shellService.init({
      initialRoute: '/carlo-header',
      outletId: 'content',
      preload: true,
      clients: {
        "carlo-header": {
          loaded: false,
          src: 'assets/micro-frontends/header/main.js',
          element: 'carlo-header',
          route: '/carlo-header'
        }
      }      
    });
  }

}
